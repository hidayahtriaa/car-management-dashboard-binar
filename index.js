// creating server with express and also using ejs as view engine
const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const app = express();
const PORT = process.env.PORT || 8000;

app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/public"));
//app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.render("pages/dashboard", {
    layout: "layouts/index",
    title: "Dashboard",
  });
});
app.get("/cars", (req, res) => {
  res.render("pages/cars", {
    layout: "layouts/index",
    title: "Cars",
  });
});

app.use((req, res) => {
  res.status(404).render("pages/404", {
    layout: "layouts/index",
    title: "404",
  });
});

app.listen(PORT, () => {
  console.log(`Server berjalan di http://localhost:${PORT}`);
});
